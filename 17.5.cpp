// 17.5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
class Vector
{
private: double x = 4; double y = 4; double z = 4;
public:
    void Show()
    {
        std::cout << x << " " << y << " " << z << '\n';
    }
    int VectorLength()
    {
        int a = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        return a;
    }

};

int main()
{
    Vector test;
    test.Show();
    std::cout << "Vector leinth is " << test.VectorLength();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
